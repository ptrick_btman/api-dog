
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Prueba entrevista</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/general.css">

    <link rel="stylesheet" href="./css/index.css">
</head>
<body>
    
</body>

    <?php
        include './views/menu.php';
        include './controller/token.php';
        include './controller/razas.php';
        
        desplegarMenu();    

        $razasA = getRazas();
        //sort($razasA[0]);

        //LOGICA FILTRADO
        $estadoSubRaza;
        $cantSubRazas=0;


        if(isset($_GET["token"]) and  isset($_GET["raza"]) and validarTokenData($_GET["token"]) ){
            $estadoSubRaza = True;
            $razasA = array_filter($razasA);
            $razasA = getSubRazas($razasA, $_GET["raza"]);

            if (empty($razasA[0])) {
                $cantSubRazas = 0;
            }else{
                $cantSubRazas = 1;
            }
            
        }else{
            $estadoSubRaza = false;
        }
    ?>

    <div class="container">
        <div class="row">
            <div class="col-12">

            </div>
        </div>

        <div class="row">
            <?php

            if($estadoSubRaza===True){
            ?>
                <div class="col-sm-12">
                    <h2>Rama principal <?php echo $_GET["raza"]; ?> </h2>

                    <?php
                        if($cantSubRazas==0){
                            echo "<h3>No se encontraron sub razas</h3>";
                        }else{?>
                            <h3>Sub Razas: </h3>
                        <?php 
                        foreach ($razasA[0] as $raza) {
                            ?>
                            <div class="col-sm-12 col-md-4">
                                <div class="box-content">
                                <p class="link" ><?php echo $raza?></p>
                                    <div class="imgRazas" style="background: -webkit-linear-gradient(70deg, rgba(0,0,0,1),transparent,transparent), url('<?php echo getImgSubRazas($_GET["raza"], $raza);?>') no-repeat;background:-webkit-gradient(70deg, rgba(0,0,0,1),transparent,transparent),url('<?php echo getImgSubRazas($_GET["raza"], $raza);?>') no-repeat !important;background:-moz-linear-gradient(70deg, rgba(0,0,0,1),transparent,transparent),url('<?php echo getImgSubRazas($_GET["raza"], $raza);?>') no-repeat;" ></div>
                                   
                                </div>
                            </div>   
                            
                            
                        <?php
                        }
                    } ?>
                </div>             
            <?php
                echo "<a href='index.php'> Ver todas las razas</a>";
            }else{
                ?>
                    <div class="col-sm-12">
                        <h2>Todas las razas</h2>
                    </div>
                <?php
                foreach ($razasA as $razaPadre=> $razasHijas) {
                    ?>
                    <div class="col-sm-12 col-md-4">
                        <div class="box-content">
                            <a  class="link" href="index.php?token=$2y$09$KR9mQukf5RvjSpV3IPUUheDfttH.cHD.YhEQTEBxgIrGIAe3xOJw2&raza=<?php echo $razaPadre?>">Ver <?php echo $razaPadre?></a><span> ></span>
                            <a href="index.php?token=$2y$09$KR9mQukf5RvjSpV3IPUUheDfttH.cHD.YhEQTEBxgIrGIAe3xOJw2&raza=<?php echo $razaPadre?>">
                                <div class="imgRazas" style="background: -webkit-linear-gradient(70deg, rgba(0,0,0,1),transparent,transparent), url('<?php echo getImgRazasPrincipal($razaPadre);?>') no-repeat;background:-webkit-gradient(70deg, rgba(0,0,0,1),transparent,transparent),url('<?php echo getImgRazasPrincipal($razaPadre);?>') no-repeat !important;background:-moz-linear-gradient(70deg, rgba(0,0,0,1),transparent,transparent),url('<?php echo getImgRazasPrincipal($razaPadre)  ;?>') no-repeat;" ></div>
                            </a>
                        </div>
                    </div>   
                    
                    
                <?php
                }

                
            }
            ?>
    
        </div>
    </div>
    <script src="./js/general.js"></script>
    <script src="./js/index.js"></script>
</html>