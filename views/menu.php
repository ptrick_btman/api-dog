<?php

function desplegarMenu(){?>

    <div class="loader" id="loader">
        <svg class="spinner" width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
            <circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle>
            
        </svg>

        <h3> &nbsp; Cargando razas ...</h3>
    </div>

    <div class="hamburger">
        <div class="stick"></div>
    </div>

    <nav class="cont-menu">
        <ul>
            <li><a href="#">Inicio</a></li>
            <li><a href="#">Sobre nosotros</a></li>
            <li><a href="#">Contacto</a></li>
        </ul>
    </nav>

<?php
}

?>