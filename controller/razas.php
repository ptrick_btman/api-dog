<?php

function getSubRazas($razas, $raza){
        $subrazas = [];
        foreach ($razas as $razaPadre => $razasHijas) {
            if($razaPadre== $raza){
                array_push($subrazas, $razasHijas);
            }
        }
        return ($subrazas);
}

// se puede optimizar 
function getRazas(){
        $curl =  curl_init('https://dog.ceo/api/breeds/list/all');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($curl, CURLOPT_TIMEOUT, 3);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/json'));
        $result = curl_exec($curl);
        $razas = json_decode($result);
        $razasA = json_decode(json_encode($razas->message), true);
        return $razasA;

    
}

function getImgRazasPrincipal ($raza){
    $curl = curl_init("https://dog.ceo/api/breed/".$raza. "/images/random");
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($curl, CURLOPT_TIMEOUT, 3);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/json'));
    $result = curl_exec($curl);
    $data = json_decode($result);
    $imgRaza = json_decode(json_encode($data->message), true);
    //return $curl    ;
    return($imgRaza);
}

function getImgSubRazas($razaPadre, $razasHija){

    $curl = curl_init("https://dog.ceo/api/breed/".$razaPadre. "/".$razasHija."/images/random");
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($curl, CURLOPT_TIMEOUT, 3);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/json'));
    $result = curl_exec($curl);
    $data = json_decode($result);
    $imgRaza = json_decode(json_encode($data->message), true);
    return $imgRaza;

}
?>